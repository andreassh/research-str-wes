# Example usage: extract-data.py --tool toolname --vcf /folder/to/vcf [--chr X --out disk/screen]

import os
import pysam
import csv
import re
import regex
import pybedtools
from argparse import ArgumentParser

def get_strs(vcf_file, output, sample, chromosome_req, genotype_req, toolname):
    with open(vcf_file) as csvfile:
        readCSV = csv.reader(csvfile, delimiter='\t')

        for row in readCSV:
            if row[0].startswith('#'):
                continue

            if toolname == 'gangstr' and row[9] == ".": # If using filtered data, then: if toolname == 'gangstr' and str(row[9])[-4:] != "PASS":
                continue

            if toolname == 'hipstr' and row[9] == ".":
                continue

            if toolname == 'lobstr' and row[9] == ".":
                continue

            if toolname == 'hipstr' and str(row[9])[-1] == ".":
                continue

            if chromosome_req is not None and row[0] != chromosome_req:
                continue

            chr = row[0]
            STR_start_pos = int(row[1])
            ref_STR_sequence = row[3]

            if toolname == 'lobstr':
                STR_end_pos = int(re.search(r'END=(.*);MOTIF', row[7]).group(1))
                motif = re.search(r'RU=(.*);VT', row[7]).group(1)
                ref_len = float(re.search(r'REF=(.*);RL', row[7]).group(1))
                STR_coverage = row[9].split(":")[4]
                quality_score = row[9].split(":")[9]

            elif toolname == 'gangstr':
                STR_end_pos = int(re.search(r'END=(.*);RU', row[7]).group(1))
                motif = re.search(r'RU=(.*);PERIOD', row[7]).group(1)
                ref_len = float(re.search(r'REF=(.*);GRID', row[7]).group(1))
                STR_coverage = row[9].split(":")[1] # All reads; if you want to have only enclosing class of reads, then: STR_coverage = row[9].split(":")[5].split(",")[0]
                CI = row[9].split(":")[4]
                quality_score = row[9].split(":")[2]

            elif toolname == 'hipstr':
                STR_end_pos = int(re.search(r'END=(.*);PERIOD', row[7]).group(1))
                motif = 'NA'
                motif_len = int(re.search(r'PERIOD=(.*);NSKIP', row[7]).group(1)) # or when filtered data: motif_len = int(re.search(r'PERIOD=(.*);AN', row[7]).group(1))
                ref_len = round(len(ref_STR_sequence)/motif_len, 1)
                STR_coverage = re.search(r'DP=(.*);DSNP', row[7]).group(1)
                quality_score = row[9].split(":")[2]

            elif toolname == 'repeatseq':
                motif = re.search(r'RU=(.*);DP', row[7]).group(1)
                ref_len = round(float(re.search(r'RL=(.*)', row[7]).group(1)) / float(len(motif)), 1)
                STR_start_pos = STR_start_pos + 1
                STR_end_pos = STR_start_pos - 1 + int(ref_len)
                ref_STR_sequence = ref_STR_sequence[1:]
                STR_coverage = re.search(r'DP=(.*);RL', row[7]).group(1)
                quality_score = 'NA'

            elif toolname == 'gatk':
                motif = 'NA'
                ref_len = 'NA'
                STR_end_pos = STR_start_pos + len(row[3])
                ref_STR_sequence = 'NA'
                STR_coverage = row[9].split(":")[2]
                detected_len_allele1 = 'NA'
                detected_len_allele2 = 'NA'
                quality_score = 'NA'

            else:
                motif = 'NA'
                ref_len = 'NA'

            detected_sequence = row[4]
            alleles = re.search(r'(.*):', str(row[9])).group(1)[0:3]

            if toolname == "lobstr":
                allreads = row[9].split(":")[1]
                allele_reads = {}

                for reads in allreads.split(";"):
                    allele_reads[int(reads.split("|")[0])] = int(reads.split("|")[1])
            else:
                allreads = "NA"
                allele_reads = "NA"

            if toolname == "hipstr":
                allreads = row[9].split(":")[15]
                allele_reads = {}

                for reads in allreads.split(";"):
                    allele_reads[int(reads.split("|")[0])] = int(reads.split("|")[1])

            if toolname == "hipstr":
                allele1_num = int(alleles.split('|')[0])
                allele2_num = int(alleles.split('|')[1])

            else:
                allele1_num = int(alleles.split('/')[0])
                allele2_num = int(alleles.split('/')[1])

            if toolname == "gangstr" or toolname == "hipstr" or toolname == "repeatseq" or (toolname == "lobstr" and detected_sequence != '.' and re.search(r'RPA=', row[7]) != None and re.search(r'RPA=-', row[7]) == None):
                if allele1_num == 0:
                    detected_sequence_allele1 = ref_STR_sequence
                elif allele1_num >= 1:
                    detected_sequence_allele1 = row[4].split(",")[allele1_num - 1]
                else:
                    detected_sequence_allele1 = 'NA'

                if allele2_num == 0:
                    detected_sequence_allele2 = ref_STR_sequence
                elif allele2_num >= 1:
                    detected_sequence_allele2 = row[4].split(",")[allele2_num - 1]
                else:
                    detected_sequence_allele2 = 'NA'

                if toolname == "lobstr":
                    detected_len = re.search(r'RPA=(.*)', row[7]).group(1)
                    if re.search(r',', detected_len):
                        detected_len_allele1 = float(detected_len.split(",")[0])
                        detected_len_allele2 = float(detected_len.split(",")[1])
                    else:
                        if allele1_num == 0:
                            detected_len_allele1 = float(ref_len)
                            detected_len_allele2 = float(detected_len.split(",")[0])
                        elif allele2_num == 0:
                            detected_len_allele1 = float(detected_len.split(",")[0])
                            detected_len_allele2 = float(ref_len)
                        else:
                            detected_len_allele1 = float(detected_len.split(",")[0])
                            detected_len_allele2 = 'NA'

                elif toolname == "gangstr":
                    detected_len = row[9].split(":")[3]

                    if re.search(r',', detected_len):
                        detected_len_allele1 = int(detected_len.split(",")[0])
                        detected_len_allele2 = int(detected_len.split(",")[1])

                    else:
                        detected_len_allele1 = int(detected_len)
                        detected_len_allele2 = 'NA'

                elif toolname == "hipstr":
                    detected_len = row[9].split(":")[1]
                    detected_len_allele1 = round(float(ref_len) + float(detected_len.split("|")[0]) / float(len(motif)), 1)
                    detected_len_allele2 = round(float(ref_len) + float(detected_len.split("|")[1]) / float(len(motif)), 1)

                elif toolname == 'repeatseq':
                    detected_len = re.search(r'AL=(.*);RU', row[7]).group(1)
                    if re.search(r',', detected_len):

                        detected_len_allele1 = round(float(ref_len) + float(detected_len.split(",")[0]) / float(len(motif)), 1)
                        detected_len_allele2 = round(float(ref_len) + float(detected_len.split(",")[1]) / float(len(motif)), 1)

                        allele1_seq_diff = abs(len(detected_sequence_allele1) - int(detected_len_allele1 * len(motif)))
                        detected_sequence_allele1 = detected_sequence_allele1[allele1_seq_diff:]

                        allele2_seq_diff = abs(len(detected_sequence_allele2) - int(detected_len_allele2 * len(motif)))
                        detected_sequence_allele2 = detected_sequence_allele2[allele2_seq_diff:]

                    else:
                        detected_len_allele1 = round(float(detected_len.group(1)), 1)
                        detected_len_allele2 = 'NA'


                if allele_reads != 'NA':
                    if re.search(r',', detected_len):
                        repeat_allele1_diff_bp = int(round(len(motif) * (detected_len_allele1-ref_len)))
                        if repeat_allele1_diff_bp in allele_reads:
                            reads_allele1 = allele_reads[repeat_allele1_diff_bp]
                        else:
                            reads_allele1 = 'NA'

                        repeat_allele2_diff_bp = int(round(len(motif) * (detected_len_allele2 - ref_len)))
                        if repeat_allele1_diff_bp in allele_reads:
                            reads_allele2 = allele_reads[repeat_allele2_diff_bp]
                        else:
                            reads_allele1 = 'NA'

                    else:
                        repeat_allele1_diff_bp = int(round(len(motif) * (detected_len_allele1-ref_len)))
                        if repeat_allele1_diff_bp in allele_reads:
                            reads_allele1 = allele_reads[repeat_allele1_diff_bp]
                        else:
                            reads_allele1 = 'NA'

                        reads_allele2 = 'NA'
                else:
                    reads_allele1 = 'NA'
                    reads_allele2 = 'NA'
            else:
                detected_len_allele1 = ref_len
                detected_len_allele2 = 'NA'
                detected_sequence_allele1 = ref_STR_sequence
                detected_sequence_allele2 = 'NA'

                if not isinstance(allele_reads, dict):
                    if allele_reads.isdigit():
                        if 0 in allele_reads:
                            reads_allele1 = allele_reads[0]
                        else:
                            reads_allele1 = 'NA'
                        reads_allele2 = 'NA'

            if allele1_num == allele2_num:
                genotype = 'HOM'
            else:
                if (toolname == 'gangstr') and detected_len_allele1 == detected_len_allele2:
                    genotype = 'HOM'
                else:
                    genotype = 'HET'

            if genotype_req is not None and genotype != genotype_req:
                continue

            if toolname == "hipstr":
                motif_len = motif_len
            else:
                motif_len = len(motif)

            out = (
                str(sample) + '\t' +
                str(chr) + ':' + str(STR_start_pos) + '-' + str(STR_end_pos) + '\t' +
                str(chr) + '\t' +
                str(STR_start_pos) + '\t' +
                str(STR_end_pos) + '\t' +
                str(motif_len) + '\t' +
                str(motif.upper()) + '\t' +
                str(ref_len) + '\t' +
                str(detected_len_allele1) + '\t' +
                str(detected_len_allele2) + '\t' +
                str(alleles) + '\t' + 
                str(genotype) + '\t' +
                str(STR_coverage) + '\t' + 
                str(quality_score) + '\n'
            )

            if output is not None:
                output.write(out)
            else:
                print(out)

def main():
    parser = ArgumentParser()
    parser.add_argument("-v", "--vcf", dest="vcf_filepath", required=False, help="VCF filepath")
    parser.add_argument("-t", "--tool", dest="tool_name", required=True, choices=['lobstr', 'gangstr', 'hipstr', 'repeatseq'], help="STR genotyping tool name")
    parser.add_argument("-c", "--chr", dest="chromosome", required=False, help="Chromosome")
    parser.add_argument("-g", "--genotype", dest="genotype", required=False, choices=['HET', 'HOM'], help="Genotype")
    parser.add_argument("-o", "--out", dest="writefile", required=False, choices=['disk', 'screen'], help="Write the output file?")

    args = parser.parse_args()
    writefile = args.writefile
    chromosome_req = args.chromosome
    genotype_req = args.genotype
    toolname = args.tool_name

    vcf_files_path = args.vcf_filepath
    output_files_path = './'

    header = 'Sample\tLocus\tChromosome\tReadStart\tReadEnd\tMotifLen\tMotif\tRefLen\tAllele1Len\tAllele2Len\tAlleles\tGenotype\tCoverage\tQualityScore\n'
    header_written = 0

    for filename in os.listdir(vcf_files_path):
        if filename.endswith(".vcf"): 
            sample_name = filename.rsplit('.', 5)[0]
            file_vcf = os.path.join(vcf_files_path, filename)


            if chromosome_req is not None:
                file_out = output_files_path + sample_name + '_' + chromosome_req + '.txt'
            else:
                file_out = output_files_path + sample_name + '.txt'

            file_combined = output_files_path + 'results_all.txt'

            print("Working on the " + sample_name + ' sample.')

            if writefile == "disk":
                with open(file_combined, 'a') as output:
                    if header_written != 1:
                        output.write(header)
                    header_written = 1
                    get_strs(file_vcf, output, sample_name, chromosome_req, genotype_req, toolname)
            elif writefile != "screen" and writefile is not None:
                with open(file_out, 'w') as output:
                    output.write(header)
                    get_strs(file_vcf, output, sample_name)
            else:
                output = None
                print(header)
                get_strs(file_vcf, output, sample_name, chromosome_req, genotype_req, toolname)
        else:
            continue

if __name__ == '__main__':
    main()

# Accuracy of STR genotyping tools in WES data

Custom made scripts as a supplementary for the [STR genotyping tools comparison article](https://doi.org/10.12688/f1000research.22639.1)